# FFXIVTTS-linux

A simple TTS python script utilising the [TextToTalk plugin](https://github.com/karashiiro/TextToTalk) & Google's gTTF to read out loud character lines from FFXIV online.

## Usage 
1. Clone this repo
2. Create a new Python virtual environment: `python -m venv _env`
3. Activate the virtual environment: `source _env/bin/activate`
4. Install dependencies: `pip install -r requirements.txt`
5. Run the main file in the shell: `python main.py` 

You can exit/stop the script from running by hitting Ctrl+C while focussing on the shell it was started in.  
Feel free to use this script however you like.

## Configuration
The websockect address, as well as the playback speed and audio volume can be configurated via the `config.json` file.  
Keep in mind that for the changes to take effect the script will have to be restarted manually.  
When using Piper the volume is handled by your OS and altering the config file will make no change.

## Piper / local TTS
To use piper (a local TTS on your machine) it must also be pre-installed, this script does not install it for you. For an easy install use [Pied](https://github.com/Elleo/pied)