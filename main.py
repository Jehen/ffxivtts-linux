import websocket
import json
import gtts
import vlc
import sys
import os

if __name__ == '__main__':
    conf = None

    def load_config():
        with open("config.json") as configFile:
            confString = configFile.read()
            config = json.loads(confString)
            return config

    def handle_input(objectID, payloadString):
        if conf["debug"]:
            print("message received")
            print(payloadString)

        payloadObj = json.loads(payloadString)

        if conf["audioHandler"]["piper"]:
            piperInterpreter(payloadObj["Payload"])
        else:
            googleInterpreter(payloadObj["Payload"])


# =========================================================== interpreters =====================================
    def piperInterpreter(text:str):
        bashCommand = 'echo "{0}"|piper-tts -m {1}{2} --output_raw | aplay -r {3} -f S16_LE -t raw'.format(text, conf["piper"]["path"], conf["piper"]["model"], int(22050*conf["media"]["playbackSpeed"]))
        os.system(bashCommand)

    def googleInterpreter(text:str):
        output = gtts.gTTS(text,lang="en",tld="ie")
        output.save('FFXIVTTStempOutput.mp3')
    
    def VLCplayback():
        p = vlc.MediaPlayer('FFXIVTTStempOutput.mp3')
        p.set_rate(conf["media"]["playbackSpeed"])
        p.audio_set_volume(conf["media"]["volume"])
        p.play()
        p.release

# ========================================================= start up sequence ====================================
    print("starting TTS listener...")
    conf = load_config()
    
    if conf == None:
        sys.exit("Missing config file!")

    if conf["debug"]:
        websocket.enableTrace(True)

    ws = websocket.WebSocketApp(conf["webSocketAddress"], on_message=handle_input)
    ws.run_forever() 
